# Description

This project is intended to provide a framework for **C++ 17/ROS2** development on **Linux** patched with **RT_PREEMPT** targetting a Raspberry Pi 4. **ROS2** is a robotics middleware used to provide soft real-time capabilities. This repo should provide a reference for the basic implementation of all dependencies and tools so it should be kept simple and thoroughly documented.

## Goals

* Provide a self-contained repo, there should be no manual downloads of dependencies.
* Provide a fully automated build process, build should always be one click.
* Provide modern tools to allow easy testing, documentation, and CI/CD of code.

# Tools

## CMake

**CMake** is the most widely used build tool for C++, there is no viable alternative here. It automates the linking of external libraries and the compilation of C++ programs. Think long and hard if you are editing the **CMake** file since it is easy to make bad architectural decisions.

**References:**
* [Main Page](https://cmake.org)
* [Modern CMake](https://cliutils.gitlab.io/modern-cmake/)

## Conan

**Conan** is a modern package manager for C++. There some room here to select a package manager of your preference. Conan was chosen as it is the only mature option that supports different versions of the same library. **NOTE:** **Conan** does have a dependency on Python.

**References:**
* [Main Page](https://conan.io/index.html)
* [CMake/Conan Wrapper](https://github.com/conan-io/cmake-conan) - Conan is only being used behind this wrapper.
* [Dependency Manager Overview](https://bincrafters.github.io/2018/07/14/What-Is-Conan/)

## Doctest

**Doctest** is lightweight testing framework for C++. From the research I've done, it provides the fastest compile times of all the options along with being a header only library.

**References:**
* [Main Page](https://github.com/onqtam/doctest)
* [Good Introduction](https://www.codeproject.com/Articles/1156938/doctest-the-lightest-Cplusplus-unit-testing-framew)
* [Tutorial](https://github.com/onqtam/doctest/blob/master/doc/markdown/tutorial.md)
* [CMake Itegration](https://github.com/onqtam/doctest/blob/master/doc/markdown/build-systems.md)
* [Conan Page](https://conan.io/center/doctest)

## Doxygen/Shpinx/Breathe

# Install

Here are the instructions to install the toolchain needed to build this project for platform:

<details>
<summary>Windows</summary>
    ew you dev on windows???
</details>
<details>
<summary>Linux</summary>
    nothing yet
</details>
<details>
<summary>macOS</summary>
<ul>
    <li>Have Python installed.</li>
    <li>Get <a href="[url](https://brew.sh)">Brew</a>.</li>
    <li><code>$ brew install cmake</code></li>
    <li><code>$ brew install conan</code></li>
    <li>SETUP In the project directory:</li>
    <li><code>$ mkdir build</code></li>
    <li><code>$ cd build</code></li>
    <li><code>$ cmake ..</code></li>
    <li><code>$ cmake --build . </code></li>
    <li>TO DO: MAKE BUILD TASK</li>
    <li>TO RUN In the project directory:</li>
    <li><code>$ cd build</code></li>
    <li><code>$ ./CppToolchainTest</code></li>
</ul>
</details>
