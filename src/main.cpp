#define DOCTEST_CONFIG_IMPLEMENT

#include <iostream>
#include <Eigen/Dense>
#include <doctest/doctest.h>
 
using namespace Eigen;
using namespace std;

/*!
  This function performs element-wise addition on two vectors.

  \f{aligned}{
    \left(\begin{array}{c}
      v_{\text{out}_1} \\
      v_{\text{out}_2} \\
      \vdots \\
      v_{\text{out}_n} \\
    \end{array}\right) = \left(\begin{array}{c}
      a_1 \\
      a_2 \\
      \vdots \\
      a_n \\
    \end{array}\right) + \left(\begin{array}{c}
      b_1 \\
      b_2 \\
      \vdots \\
      b_n \\
    \end{array}\right)
  \f}
*/
static VectorXd addVectors(VectorXd a, VectorXd b) {
  VectorXd vOut = a + b;
  return vOut;
}

int main(int argc, char** argv) {
  doctest::Context ctx;
  ctx.setOption("abort-after", 5);
  ctx.applyCommandLine(argc, argv);
  ctx.setOption("no-breaks", true); 
  int res = ctx.run();
  if(ctx.shouldExit())
    return res;

  VectorXd v1(3);
  v1 << 1, 2, 3;
  VectorXd v2(3);
  v2 << 2, 3, 4;

  VectorXd vOut = addVectors(v1, v2);

  cout << "v1 + v2 =" << endl << vOut << endl;
  return 0;
}

TEST_CASE("addVectors") {
    VectorXd v1(3);
    v1 << 1, 2, 3;
    VectorXd v2(3);
    v2 << 2, 3, 4;
    VectorXd vExpect(3);
    vExpect << 3, 5, 7;

    CHECK(addVectors(v1, v2) == vExpect);
}
